<?php

/**
 * @file
 * Block template override for content-bottom theme region.
 *
 * This custom theme region is defined in wwuzen-westerntoday.
 */
?>
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="center-content">
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php print $content; ?>
  <div>
</div>
