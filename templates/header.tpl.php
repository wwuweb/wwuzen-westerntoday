<header class="page-title">
  <?php print render($variables["title_prefix"]); ?>
  <?php if ($variables["title"]): ?>
    <h1><?php print $variables["title"]; ?></h1>
  <?php endif; ?>
  <?php print $variables["breadcrumb"]; ?>
</header>
