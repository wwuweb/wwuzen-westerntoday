<?php

/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * A QUICK OVERVIEW OF DRUPAL THEMING
 *
 *   The default HTML for all of Drupal's markup is specified by its modules.
 *   For example, the comment.module provides the default HTML markup and CSS
 *   styling that is wrapped around each comment. Fortunately, each piece of
 *   markup can optionally be overridden by the theme.
 *
 *   Drupal deals with each chunk of content using a "theme hook". The raw
 *   content is placed in PHP variables and passed through the theme hook, which
 *   can either be a template file (which you should already be familiary with)
 *   or a theme function. For example, the "comment" theme hook is implemented
 *   with a comment.tpl.php template file, but the "breadcrumb" theme hooks is
 *   implemented with a theme_breadcrumb() theme function. Regardless if the
 *   theme hook uses a template file or theme function, the template or function
 *   does the same kind of work; it takes the PHP variables passed to it and
 *   wraps the raw content with the desired HTML markup.
 *
 *   Most theme hooks are implemented with template files. Theme hooks that use
 *   theme functions do so for performance reasons - theme_field() is faster
 *   than a field.tpl.php - or for legacy reasons - theme_breadcrumb() has "been
 *   that way forever."
 *
 *   The variables used by theme functions or template files come from a handful
 *   of sources:
 *   - the contents of other theme hooks that have already been rendered into
 *     HTML. For example, the HTML from theme_breadcrumb() is put into the
 *     $breadcrumb variable of the page.tpl.php template file.
 *   - raw data provided directly by a module (often pulled from a database)
 *   - a "render element" provided directly by a module. A render element is a
 *     nested PHP array which contains both content and meta data with hints on
 *     how the content should be rendered. If a variable in a template file is a
 *     render element, it needs to be rendered with the render() function and
 *     then printed using:
 *       <?php print render($variable); ?>
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. With this file you can do three things:
 *   - Modify any theme hooks variables or add your own variables, using
 *     preprocess or process functions.
 *   - Override any theme function. That is, replace a module's default theme
 *     function with one you write.
 *   - Call hook_*_alter() functions which allow you to alter various parts of
 *     Drupal's internals, including the render elements in forms. The most
 *     useful of which include hook_form_alter(), hook_form_FORM_ID_alter(),
 *     and hook_page_alter(). See api.drupal.org for more information about
 *     _alter functions.
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   If a theme hook uses a theme function, Drupal will use the default theme
 *   function unless your theme overrides it. To override a theme function, you
 *   have to first find the theme function that generates the output. (The
 *   api.drupal.org website is a good place to find which file contains which
 *   function.) Then you can copy the original function in its entirety and
 *   paste it in this template.php file, changing the prefix from theme_ to
 *   wwuzen_. For example:
 *
 *     original, found in modules/field/field.module: theme_field()
 *     theme override, found in template.php: wwuzen_field()
 *
 *   where wwuzen is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_field() function.
 *
 *   Note that base themes can also override theme functions. And those
 *   overrides will be used by sub-themes unless the sub-theme chooses to
 *   override again.
 *
 *   Zen core only overrides one theme function. If you wish to override it, you
 *   should first look at how Zen core implements this function:
 *     theme_breadcrumbs()      in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called theme hook suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node--forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and theme hook suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440 and http://drupal.org/node/1089656
 */

/**
 * Implements theme_preprocess_html().
 */
function wwuzen_westerntoday_preprocess_html(&$variables, $hook) {
  // Add the slide effect for our search box and wwumenu.
  drupal_add_library('system', 'ui');
}

/**
 * Implements hook_theme().
 */
function wwuzen_westerntoday_theme($existing, $type, $theme, $path) {
  $themes = array();
  $themes['inner_page_header'] = array(
    'variables' => array(
      'breadcrumb' => NULL,
      'title_prefix' => NULL,
      'title' => NULL,
    ),
    'template' => 'templates/header',
  );
  return $themes;
}

/**
 * Implements theme_preprocess_page().
 */
function wwuzen_westerntoday_preprocess_page(&$variables, $hook) {
  $theme_path = drupal_get_path('theme', 'wwuzen_westerntoday');
  $variables['department_name'] = theme_get_setting('department_name');

  /* Time of day changing header images */
  $hour = date('G');

  // Midnight to 5:59 am.
  $index = 0;

  if ($hour >= 6  && $hour < 12) {
    // 6am to 11:59am.
    $index = 1;
  }
  elseif ($hour < 18) {
    // Noon to 5:59 pm.
    $index = 2;
  }
  elseif ($hour < 24) {
    // 6pm to midnight.
    $index = 3;
  }

  $variables['logo'] = file_create_url($theme_path . '/images/changing-headers/HT' . $index . '.jpg');
}

/**
 * Implements theme_process_page().
 *
 * Add variables necessary to render header template.
 */
function wwuzen_westerntoday_process_page(&$variables) {
  $variables['header_vars'] = [
    "#theme" => "inner_page_header",
    "#title" => $variables["title"],
    "#breadcrumb" => $variables["breadcrumb"],
    "#title_prefix" => $variables["title_prefix"],
  ];
}

/**
 * Implements template_preprocess_views_view_rss().
 *
 * Alter the main feed link generated by Views feeds.
 */
function wwuzen_westerntoday_preprocess_views_view_rss(&$vars) {
  $vars['link'] = 'https://westerntoday.wwu.edu/';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function wwuzen_westerntoday_form_mailchimp_signup_subscribe_block_get_our_newsletter_form_alter(&$form, &$form_state, $form_id) {
  unset($form['mergevars']['EMAIL']['#default_value']);
  $form['mergevars']['EMAIL']['#attributes'] = array(
    'placeholder' => array('email address'),
  );
}
